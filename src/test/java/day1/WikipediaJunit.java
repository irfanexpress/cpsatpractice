package day1;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utils.HelperFunctions;

public class WikipediaJunit {
	
	WebDriver driver;

	@Before
	public void setUp() throws Exception {

		driver = HelperFunctions.createAppropriateDriver("chrome");

	}


	@Test
	public void test() throws InterruptedException {
		//fail("Not yet implemented");
		driver.get("https://www.wikipedia.org/");


		// Click on English
		// //*[@id="js-link-box-en"]/strong
		//*[@id="js-link-box-en"]/strong
		
		
		By byEnglishLink = By.xpath("//*[@id=\"js-link-box-en\"]/strong");
		WebElement we =  driver.findElement(byEnglishLink);
		we.click();
		Thread.sleep(2000);
		
		// Search for Selenium and then click on the magnifying glass
		// //*[@id="searchInput"]
		By bySearch = By.xpath("//*[@id=\"searchInput\"]");
		WebElement weSearch = driver.findElement(bySearch);
		weSearch.sendKeys("Selenium");
		
		// //*[@id="searchButton"]
		By bySrchButton = By.xpath("//*[@id=\"searchButton\"]");
		WebElement weSearchButton = driver.findElement(bySrchButton);
		weSearchButton.click();
		System.out.println(driver.getTitle());
		Thread.sleep(2000);			
	}

	@After
	public void tearDown() throws Exception {
		
		driver.quit();
	}

}
