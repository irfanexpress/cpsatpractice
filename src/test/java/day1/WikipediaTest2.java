package day1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import utils.HelperFunctions;

public class WikipediaTest2 {

	public static void main(String[] args) throws InterruptedException {


		WebDriver driver = HelperFunctions.createAppropriateDriver("chrome");

		//WebDriver driver = new FirefoxDriver();

		driver.get("https://www.wikipedia.org/");

		
		By byEnglishLink = By.xpath("//*[@id=\"js-link-box-en\"]/strong");
		WebElement we =  driver.findElement(byEnglishLink);
		we.click();
		Thread.sleep(2000);
		
		// Search for Selenium and then click on the magnifying glass
		// //*[@id="searchInput"]
	//	By byArticles = By.xpath("//*[@id=\"articlecount\"]");
		
		System.out.println(driver.findElement(By.xpath("//*[@id=\"articlecount\"]//*[@title=\"Special:Statistics\"]")).getText());
	

		
		Thread.sleep(2000);	
		driver.quit();
	
	}

}
