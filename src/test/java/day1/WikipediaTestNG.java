package day1;

import org.testng.annotations.Test;

import utils.HelperFunctions;

import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;

public class WikipediaTestNG {

	WebDriver driver;
	  @BeforeTest
	  public void beforeTest() {

			driver = HelperFunctions.createAppropriateDriver("chrome");

			//WebDriver driver = new FirefoxDriver();

			driver.get("https://www.wikipedia.org/");
	  }
	
	
	@Test
  public void f() throws InterruptedException {

		// Click on English
		// //*[@id="js-link-box-en"]/strong
		//*[@id="js-link-box-en"]/strong
		
		
		By byEnglishLink = By.xpath("//*[@id=\"js-link-box-en\"]/strong");
		WebElement we =  driver.findElement(byEnglishLink);
		we.click();
		Thread.sleep(2000);
		
		// Search for Selenium and then click on the magnifying glass
		// //*[@id="searchInput"]
		By bySearch = By.xpath("//*[@id=\"searchInput\"]");
		WebElement weSearch = driver.findElement(bySearch);
		weSearch.sendKeys("Selenium");
		
		// //*[@id="searchButton"]
		By bySrchButton = By.xpath("//*[@id=\"searchButton\"]");
		WebElement weSearchButton = driver.findElement(bySrchButton);
		weSearchButton.click();
		System.out.println(driver.getTitle());
		Thread.sleep(2000);	
  }

  @AfterTest
  public void afterTest() {
	  driver.quit();
  }

}
