package day1;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import utils.HelperFunctions;

public class NavigationExercise {

	public static void main(String[] args) {

		WebDriver driver = HelperFunctions.createAppropriateDriver("chrome");

		driver.get("https://www.ataevents.org/");

		
		// scroll element
		
		//*[@id="main-home-content"]/div[4]/div/div/div/div/div/div[1]/ul/li[1]/a/span
		
		WebElement elementToScroll = driver.findElement(By.xpath("//*[@id=\"main-home-content\"]/div[4]/div/div/div/div/div/div[1]/ul/li[1]/a/span"));				

		scrollIntoView(driver,elementToScroll);

		
		//*[@id="india"]/div[2]/div[1]/div[1]/div/div/div[1]/figure/a
		By byFirstEvent = By.xpath("//*[@id=\"india\"]/div[2]/div[1]/div[1]/div/div/div[1]/figure/a");

		WebElement we = driver.findElement(byFirstEvent);

		we.click();

		System.out.println(driver.getTitle()); 

		Set<String> iter = driver.getWindowHandles();

		int numHandles = iter.size();
		
		System.out.println("numhandles are " + numHandles);

		HelperFunctions.switchToWindow(driver,"Selenium CPSAT");

		System.out.println(driver.getTitle()); 

		driver.quit();


	}
	
	// Scroll element to view
	public static void scrollIntoView(WebDriver driver,WebElement element ) {
		try {
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("arguments[0].scrollIntoView(true);", element);
		} catch (Exception e){
			System.out.println("=============================================================");
			HelperFunctions.captureScreenShot(driver, "src\\test\\resources\\screenshots\\scrollexception.jpg");
			e.printStackTrace();
			System.out.println("=============================================================");
		}    
	}		

}
