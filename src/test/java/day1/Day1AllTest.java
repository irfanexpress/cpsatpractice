package day1;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ NavigationJunit.class, WikipediaJunit.class })
public class Day1AllTest {

}
