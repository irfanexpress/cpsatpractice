package day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.HelperFunctions;

public class AnnaUniv1 {

	public static void main(String[] args) {

		WebDriver driver;
		
		driver = HelperFunctions.createAppropriateDriver("chrome");
		
		driver.get("https://www.annauniv.edu/");

		//https://www.annauniv.edu/

		// Departments
		
	   
		//By byLink = By.linkText("Departments");
		
		By byLink = By.partialLinkText("Depart");

		
		WebElement ele1 = driver.findElement(byLink);

		try {
			
			ele1.click();
			//org.openqa.selenium.ElementClickInterceptedException
		}
		catch (ElementClickInterceptedException exp) {
			
			// if there is an exception due to overlay issues
			// directly use JavaScript click
			WebElement ele = driver.findElement(byLink);
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", ele);
		}

		//*[@id="link3"]/strong

		//*[@id="link3"]/strong

		By byDepartofCivil = By.xpath("//*[@id=\"link3\"]/strong");

		WebElement ele2 = driver.findElement(byDepartofCivil);

		Actions action = new Actions(driver);
		
		action.moveToElement(ele2);
	
		action.perform();

		/*
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		
		WebDriverWait wait = new WebDriverWait(driver, 10);

		By byHoverObj = By.xpath("//*[@id=\"menuItemHilite32\"]");
		
		try {
			
			wait.until(ExpectedConditions.presenceOfElementLocated(byHoverObj));
			
		}
		catch (Exception e) {
			
			System.out.println(e.getMessage());
		}
		
		
		
		//hover - institute of ocean management
		//*[@id="menuItemHilite32"]


		
		WebElement ele3 = driver.findElement(byHoverObj);

		action.moveToElement(ele3);
		action.perform();

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		action.click();
		action.perform();

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println(driver.getTitle());

		driver.quit();
	}

}
