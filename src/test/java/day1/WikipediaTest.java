package day1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import utils.HelperFunctions;

public class WikipediaTest {

	public static void main(String[] args) throws InterruptedException {


		WebDriver driver = HelperFunctions.createAppropriateDriver("chrome");

		//WebDriver driver = new FirefoxDriver();

		driver.get("https://www.wikipedia.org/");


		// Click on English
		// //*[@id="js-link-box-en"]/strong
		//*[@id="js-link-box-en"]/strong
		
		
		By byEnglishLink = By.xpath("//*[@id=\"js-link-box-en\"]/strong");
		WebElement we =  driver.findElement(byEnglishLink);
		we.click();
		Thread.sleep(2000);
		
		// Search for Selenium and then click on the magnifying glass
		// //*[@id="searchInput"]
		By bySearch = By.xpath("//*[@id=\"searchInput\"]");
		WebElement weSearch = driver.findElement(bySearch);
		weSearch.sendKeys("Selenium");
		
		// //*[@id="searchButton"]
		By bySrchButton = By.xpath("//*[@id=\"searchButton\"]");
		WebElement weSearchButton = driver.findElement(bySrchButton);
		weSearchButton.click();
		System.out.println(driver.getTitle());
		Thread.sleep(2000);	
		driver.quit();
	
	}

}
