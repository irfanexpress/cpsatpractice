package day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.HelperFunctions;

public class AlertHandling {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebDriver driver;

		driver = HelperFunctions.createAppropriateDriver("chrome");
		
		driver.get("https://the-internet.herokuapp.com/javascript_alerts");

		// Click on alert button number 1
		//*[@id="content"]/div/ul/li[1]/button

		driver.findElement(By.xpath("//*[@id=\"content\"]/div/ul/li[1]/button")).click();

		WebDriverWait wait = new WebDriverWait(driver, 10);

		wait.until(ExpectedConditions.alertIsPresent());

		Alert alert = driver.switchTo().alert();

		alert.accept();

		System.out.println(driver.findElement(By.xpath("//*[@id=\"result\"]")).getText());

		// Click on alert button number 2
		// //*[@id="content"]/div/ul/li[2]/button

		driver.findElement(By.xpath("//*[@id=\"content\"]/div/ul/li[2]/button")).click();

		// click on cancel

		wait.until(ExpectedConditions.alertIsPresent());

		alert = driver.switchTo().alert();

		alert.dismiss();

		System.out.println(driver.findElement(By.xpath("//*[@id=\"result\"]")).getText());

		// Click on alert button number 3
		// //*[@id="content"]/div/ul/li[3]/button
		// //*[@id="content"]/div/ul/li[3]/button

		driver.findElement(By.xpath("//*[@id=\"content\"]/div/ul/li[3]/button")).click();

		wait.until(ExpectedConditions.alertIsPresent());

		alert = driver.switchTo().alert();
		
		alert.sendKeys("Aditya Garg");

		alert.accept();

		System.out.println(driver.findElement(By.xpath("//*[@id=\"result\"]")).getText());
		
		driver.quit();


	}

}
