package day1;

import org.testng.annotations.Test;

import utils.HelperFunctions;
import utils.MoreHelperFunctions;

import org.testng.annotations.BeforeTest;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;

public class NavigationTestNG {
	
	WebDriver driver;

	@BeforeTest
	public void beforeTest() {

		driver = HelperFunctions.createAppropriateDriver("chrome");
		driver.get("https://www.ataevents.org/");		
	}


	@Test
	public void f() {

		// scroll element
		
		//*[@id="main-home-content"]/div[4]/div/div/div/div/div/div[1]/ul/li[1]/a/span
		
		//WebElement elementToScroll = driver.findElement(By.xpath("//*[@id=\"main-home-content\"]/div[4]/div/div/div/div/div/div[1]/ul/li[1]/a/span"));				
		WebElement elementToScroll = driver.findElement(By.xpath("//div[@class=\"copyright-bar\"]"));				

		MoreHelperFunctions.scrollIntoView(driver,elementToScroll);
		
		//*[@id="india"]/div[2]/div[1]/div[1]/div/div/div[1]/figure/a
		//By byFirstEvent = By.xpath("//*[@id=\"india\"]/div[2]/div[1]/div[1]/div/div/div[1]/figure/a");

		//*[@class='elementor-image-box-title'][1]
		By byFirstEvent = By.xpath("//*[@class=\"elementor-image-box-title\"][1]");
		
		WebElement we = driver.findElement(byFirstEvent);

		we.click();

		System.out.println(driver.getTitle()); 

		Set<String> iter = driver.getWindowHandles();

		int numHandles = iter.size();
		
		System.out.println("numhandles are " + numHandles);

		HelperFunctions.switchToWindow(driver,"Selenium CPSAT");

		System.out.println(driver.getTitle()); 
		
	}


	@AfterTest
	public void afterTest() {
		
		driver.quit();
	}

	
}
