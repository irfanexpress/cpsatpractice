package day3;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ATACalcPOM02 {

	 By byField1 = By.xpath("//*[@id=\"ID_nameField1\"]");
	 By byField2 = By.xpath("//*[@id=\"ID_nameField2\"]");
	 By byMulOption = By.xpath("//*[@id=\"gwt-uid-2\"]");
	 By byCalcButton = By.xpath("//*[@id=\"ID_calculator\"]");
	 By byResult = By.xpath("//*[@id=\"ID_nameField3\"]");
	
	WebDriver driver;
	
	public ATACalcPOM02(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}
	
	
	public String multiply(String st1, String st2) {
		
		String result = null;
		
		driver.findElement(byField1).clear();
		driver.findElement(byField1).sendKeys(st1);
		driver.findElement(byField2).clear();
		driver.findElement(byField2).sendKeys(st2);
		driver.findElement(byMulOption).click();
		
		driver.findElement(byCalcButton).click();
		
		result = driver.findElement(byResult).getAttribute("value");
		
		return result;
	}

}
