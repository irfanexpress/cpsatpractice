package day3;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class ATACalcDay3 {
	
	WebDriver driver;
  @Test
  public void f() {
	  
	//*[@id="ID_nameField1"]
	 
	//*[@id="ID_nameField2"]
	  
	//*[@id="gwt-uid-2"]
	  
	//*[@id="ID_calculator"]
	  
	  
	//*[@id="ID_nameField3"]
	  
	 By byField1 = By.xpath("//*[@id=\"ID_nameField1\"]");
	 By byField2 = By.xpath("//*[@id=\"ID_nameField2\"]");
	 By byMulOption = By.xpath("//*[@id=\"gwt-uid-2\"]");
	 By byCalcButton = By.xpath("//*[@id=\"ID_calculator\"]");
	 By byResult = By.xpath("//*[@id=\"ID_nameField3\"]");
	 
	 WebElement weField1 = driver.findElement(byField1);
	 WebElement weField2 = driver.findElement(byField2);
	 WebElement weMulOption = driver.findElement(byMulOption);
	 WebElement weCalc = driver.findElement(byCalcButton);
	 WebElement weResult = driver.findElement(byResult);
	 
	 String strValue1 = "12";
	 String strValue2 = "12";
	 int expectedResult = 143;
	 
	 weField1.clear();
	 weField1.sendKeys(strValue1);
	 weField2.clear();
	 weField2.sendKeys(strValue2);
	 
	 weMulOption.click();
	 
	 weCalc.click();
	 
	 String strActualResult = weResult.getAttribute("value");
	 
	 System.out.println("Actual Result = " + strActualResult);
	 
 
	 int actualIntValue = Integer.parseInt(strActualResult);
	 
	 Assert.assertEquals(actualIntValue, expectedResult,"Error in actual result");
	 
	  
	  
  }
  @BeforeTest
  public void beforeTest() {
	  
		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");

		driver = new ChromeDriver();

		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		driver.get("http://ata123456789123456789.appspot.com/");
  }

  @AfterTest
  public void afterTest() {
	  driver.quit();
  }

}
