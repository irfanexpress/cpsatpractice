package day3;

import org.testng.annotations.Test;

import utils.HelperFunctions;

import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;


public class ATACalcTestUsesPom02 {

	WebDriver driver;

	@BeforeTest
	public void beforeTest() {

		driver = HelperFunctions.createAppropriateDriver("chrome");
		driver.get("http://ata123456789123456789.appspot.com/");

	}
	
	
	@Test
	public void f() {

		ATACalcPOM02 objCalc = new ATACalcPOM02(driver);
		//ATACalcPageFactory03 objCalc = new ATACalcPageFactory03(driver);
		String strValue1 = "12";
		String strValue2 = "12";
		int expectedResult = 143;
		String actualResultStr = objCalc.multiply(strValue1, strValue2);
		
		int actualIntValue = Integer.parseInt(actualResultStr);
		 
		 Assert.assertEquals(actualIntValue, expectedResult,"Error in actual result");
		
	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}


}
