package day3;

import org.testng.annotations.Test;

import utils.HelperFunctions;

import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class ATACalcTNG01 {

	WebDriver driver;	

	@BeforeTest
	public void beforeTest() {

		driver = HelperFunctions.createAppropriateDriver("chrome");
		driver.get("http://ata123456789123456789.appspot.com/");
	}
	
	@Test
	public void f() throws InterruptedException {
	
		// Locators for ATACalc
		//*[@id="ID_nameField1"]
		//*[@id="ID_nameField2"]
		//*[@id="gwt-uid-2"]
		//*[@id="ID_calculator"]
		//*[@id="ID_nameField3"]
		
		By byF1 = By.xpath("//*[@id='ID_nameField1']");
		By byF2 = By.xpath("//*[@id='ID_nameField2']");
		By byOption = By.xpath("//*[@id='gwt-uid-2']");
		By byButton = By.xpath("//*[@id='ID_calculator']");
		By byResult = By.xpath("//*[@id='ID_nameField3']");
		
		WebElement field1WE = driver.findElement(byF1);
		WebElement field2WE = driver.findElement(byF2);
		WebElement optionWE = driver.findElement(byOption);
		WebElement buttonWE = driver.findElement(byButton);
		WebElement resultWE = driver.findElement(byResult);
		
		String number1 = "12";
		String number2 = "12";
		int expectedResult = 144;

		field1WE.clear();
		field1WE.sendKeys(number1);
		field2WE.clear();
		field2WE.sendKeys(number2);
		
		optionWE.click();
		
		buttonWE.click();
		
		//sleep for our debugging purposes
		
		Thread.sleep(2000);

		String result = resultWE.getAttribute("value");
		System.out.println(result);
		//convert string to integer
		int intActualResult = Integer.parseInt(result);
		
		
		boolean calculationCorrect = false;
		if (intActualResult == expectedResult) {
			
			calculationCorrect = true;
		}

		
		
		Assert.assertTrue(calculationCorrect, "Expected Result was " + expectedResult 
				+ " Actual Result is " + intActualResult);
		
		
		Assert.assertEquals( intActualResult,expectedResult, "The results do not match");
		
		System.out.println("Expected Result was " + expectedResult 
					+ " , Actual Result is = " + intActualResult);
	}


	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}
