package day3;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ATACalcPageFactory03 {


	//By byField1 = By.xpath();

	@FindBy(xpath= "//*[@id=\"ID_nameField1\"]") 
	WebElement field1we ;	

	// By byField2 = By.xpath();

	@FindBy(xpath= "//*[@id=\"ID_nameField2\"]") 
	WebElement field2we ;	


	//By byMulOption = By.xpath();

	@FindBy(xpath= "//*[@id=\"gwt-uid-2\"]") 
	WebElement weMultiply ;	

	//By byCalcButton = By.xpath();

	@FindBy(xpath= "//*[@id=\"ID_calculator\"]") 
	WebElement weCalc ;	

	// By byResult = By.xpath();

	@FindBy(xpath= "//*[@id=\"ID_nameField3\"]") 
	WebElement weResult ;	

	WebDriver driver;



	public ATACalcPageFactory03(WebDriver driver) {
		// TODO Auto-generated constructor stub
		PageFactory.initElements(driver,this);
		this.driver = driver;
		
	}
	
	public String multiply(String st1, String st2) {
		
		String result = null;
		
		field1we.clear();
		field1we.sendKeys(st1);
		field2we.clear();
		field2we.sendKeys(st2);
		weMultiply.click();
		
		weCalc.click();
		
		result = weResult.getAttribute("value");
		
		return result;
	}


}
