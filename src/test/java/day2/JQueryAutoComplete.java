package day2;

import org.testng.annotations.Test;

import utils.HelperFunctions;

import org.testng.annotations.BeforeTest;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;


public class JQueryAutoComplete {

	WebDriver driver;

	@BeforeTest
	public void beforeTest() {

		driver = HelperFunctions.createAppropriateDriver("chrome");

	}

	@Test
	public void f() {

		driver.get("https://jqueryui.com/autocomplete/");

		// To handle the frame (Using by index)
		//driver.switchTo().frame(0);

		// to handle frame using webElement
		//*[@class="demo-frame"]
		By byFrame = By.className("demo-frame");
		WebElement eleFrame = driver.findElement(byFrame);

		driver.switchTo().frame(eleFrame);

		//driver.switchTo().f

		WebElement element = driver.findElement(By.className("ui-autocomplete-input"));
		element.sendKeys("j");

		// //*[@id="ui-id-1"]
		// //*[@id="ui-id-1"]/descendant::*

		// //*[@id="ui-id-1"]/li


		WebDriverWait wait = new WebDriverWait(driver, 10);
		By byDynamicList = By.xpath("//*[@id='ui-id-1']/li");

		wait.until(
				ExpectedConditions.visibilityOfAllElementsLocatedBy
				(byDynamicList));

		List<WebElement> lst = driver.findElements(byDynamicList);

		//utility.HelperFunctions.hardSleep(2000);

		System.out.println(lst.size());

		for(int i=0; i < lst.size(); i++){

			WebElement ele = lst.get(i);

			if(ele.getText().equals("Java")){
				System.out.println("Found Java at index " +  i);
				ele.click();
				break;

			}
			else {
				System.out.println("Not Found " + i);
				System.out.println(ele.getText());
			}
		}
		//utility.HelperFunctions.hardSleep(2000);

	}



	@AfterTest
	public void afterTest() {

		driver.quit();
	}

}
