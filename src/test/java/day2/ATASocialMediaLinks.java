package day2;

import org.testng.annotations.Test;

import utils.HelperFunctions;

import utils.MoreHelperFunctions;


import org.testng.annotations.BeforeTest;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;

public class ATASocialMediaLinks {
	
 WebDriver driver;	

 @BeforeTest
 public void beforeTest() {
	  
		driver = HelperFunctions.createAppropriateDriver("chrome");
		driver.get("http://agiletestingalliance.org/");
 }

  @Test
  public void f() {
	  
	// /html/body/footer/div/a[*]

	  
	  
		By allElementsImages = By.xpath("//*[@class='elementor-grid-item']/a");
		
		// scroll to this element
		MoreHelperFunctions.scrollIntoView(driver, driver.findElement(allElementsImages));
		
		// Take a screen shot
		HelperFunctions.captureScreenShot(driver, "src\\test\\resources\\screenshots\\atasocialmedia.jpg");

		List <WebElement> weList = driver.findElements(allElementsImages);
		
		int size = weList.size();
		
		System.out.println("Size of the list is = " + size );
		
		for (int i = 0; i <size; i++) {
		
			WebElement weofList = weList.get(i);
			String strLink = weofList.getAttribute("href");
			
			System.out.println("Link to Social Media is " + strLink);
			
		}	  
		
	
		
		
  }

  @AfterTest
  public void afterTest() {
	  
	  driver.quit();
  }

}
