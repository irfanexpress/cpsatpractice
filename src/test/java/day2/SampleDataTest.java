package day2;

import org.testng.annotations.Test;

import utils.DataReaders;
import utils.HelperFunctions;

import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeTest;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;

public class SampleDataTest {

	 WebDriver driver;	

	@BeforeTest
	public void beforeTest() {
		driver = HelperFunctions.createAppropriateDriver("chrome");
		
	}

	@Test(dataProvider = "dp")
	public void f(String parameter1) {
		
		System.out.println("Param1 = " +  parameter1);
		driver.get("https://google.com/");
		
		By search = By.name("q");
		driver.findElement(search).sendKeys(parameter1);
		driver.findElement(search).sendKeys(Keys.ENTER);
		
		
		
	}
	

	// DataProvider creates or returns a 2 dimensional array - it can be normal or complex data structures
	// rows * columns
	// more like an excel sheet
	
	@DataProvider
	public Object[][] dp() {
/*		return new Object[][] {
			new Object[] { "CPSAT"},
			new Object[] { "CPMAT" },
			new Object[] { "CPDOF"},
		};
*/
		String[][] values = null;
		try {
			values = DataReaders.getExcelDataUsingPoi("src\\test\\resources\\data\\GoogleSearchData.xlsx", "data");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return values;
	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}
