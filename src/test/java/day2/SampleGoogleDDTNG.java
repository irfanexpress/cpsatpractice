package day2;

import org.testng.annotations.Test;

import utils.DataReaders;
import utils.HelperFunctions;

import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeTest;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;

public class SampleGoogleDDTNG {

	WebDriver driver;	
	@BeforeTest
	public void beforeTest() {
		driver = HelperFunctions.createAppropriateDriver("chrome");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}


	@Test(dataProvider = "dp")
	public void f(String searchString) {

		driver.get("https://www.google.com/");
		System.out.println("Parameter 1 " + searchString);
		By byInputText = By.name("q");

		WebElement we =  driver.findElement(byInputText);

		//driver.findElement(byInputText).    sendKeys("CPSAT");	
		// replace CPSAT with the data coming in the parameter searchString

		we.sendKeys(searchString);

		we.sendKeys(Keys.ENTER);

	}

	@DataProvider
	public Object[][] dp() {

		String newArray[][] = 
			{ 
					{"CPSAT"},
					{"CPDOF"},
					{"CPAAT"},
					{"CPMLDS"},
			};

		return newArray;

	}

	// create a new test and data provider which will search the words from a data file

	@Test(dataProvider = "dpfromfile")
	public void file(String searchString) {

		driver.get("https://www.google.com/");
		System.out.println("Parameter 1 " + searchString);
		By byInputText = By.name("q");

		WebElement we =  driver.findElement(byInputText);

		//driver.findElement(byInputText).    sendKeys("CPSAT");	
		// replace CPSAT with the data coming in the parameter searchString

		we.sendKeys(searchString);

		we.sendKeys(Keys.ENTER);

	}

	@DataProvider
	public Object[][] dpfromfile() {

		String newArray[][] = null;

		try {
			newArray = DataReaders.getExcelDataUsingPoi("src\\test\\resources\\data\\GoogleSearchData.xlsx", "data");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return newArray;

	}	

	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}
