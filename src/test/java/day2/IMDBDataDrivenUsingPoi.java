package day2;

import org.testng.annotations.Test;

import utils.DataReaders;
import utils.HelperFunctions;
import utils.MoreHelperFunctions;

import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeTest;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class IMDBDataDrivenUsingPoi {

	WebDriver driver;	
	
	@BeforeTest
	public void beforeTest() {
		driver = HelperFunctions.createAppropriateDriver("chrome");
	}

	
	@Test(dataProvider = "dp")

	public void f(String movieName, 
			String directorName,
			String starName) {

		driver.get("https://www.imdb.com/");
		
		System.out.println("Parameter 1 " + movieName);
		System.out.println("Parameter 2 " + directorName);
		System.out.println("Parameter 3 " + starName);
		//input[@id='suggestion-search']
		By bySearchMovie = By.xpath("//input[@id='suggestion-search']");
		WebElement weSearchMovie =  driver.findElement(bySearchMovie);
		weSearchMovie.sendKeys(movieName);
		
		// Click on the magnifying button
		//*[@id="navbar-submit-button"]
		
		//button[@id='suggestion-search-button']//*[local-name()='svg']
		By byMagnifyingGlass = By.xpath("//button[@id='suggestion-search-button']//*[local-name()='svg']");
		WebElement weMag =  driver.findElement(byMagnifyingGlass);
		weMag.click();
		
		// click on the movie name
		
		//*[@class="result_text"]/a[1]
		
		// Please check if the movie exists or not
		
		By results = By.xpath("//*[@class=\"result_text\"]/a[1]");
		
		String movieResult = driver.findElement(results).getText();
		
		if (movieResult.contains(movieName))
		{
			// movie is there
			// click
			driver.findElement(results).click();
			System.out.println("Movie name found = " + movieResult);
			
		}
		else {
			System.out.println("Movie name not found: value = " +  movieResult);
			Assert.fail("Movie link not found");
		}
						
		
		// fetch the director name and actor names now
		
		String actualDirectorName = "";

		// scroll to Director
		
		//By byDirectorLabel =  By.xpath("//*[@class=\"inline\" and contains(text(),\"Directors\")]");
		By byDirectorLabel =  By.xpath("//div[@class=\"PrincipalCredits__PrincipalCreditsPanelWideScreen-hdn81t-0 iGxbgr\"] //*[contains(text(),\"Directors\")]");
		MoreHelperFunctions.scrollIntoView(driver, driver.findElement(byDirectorLabel));
		
		//By byDirector = By.xpath("//*[@class=\"inline\" and contains(text(),\"Director\")]/following-sibling::*");
		By byDirector = By.xpath("//div[@class=\"PrincipalCredits__PrincipalCreditsPanelWideScreen-hdn81t-0 iGxbgr\"] //*[contains(text(),\"Directors\")]/following-sibling::*");
		
		actualDirectorName = driver.findElement(byDirector).getText();
		
		System.out.println("Director name is = " + actualDirectorName );
		
		boolean flagDirector = false;
		if (actualDirectorName.contains(directorName))
		{
			System.out.println("Director name is correct");
			flagDirector = true;
		}
		else {
			System.out.println("Director name is WRONG");
			flagDirector = false;
		}
		
		Assert.assertTrue(flagDirector, "Director name not found");
		
		// check the actor names now
		
		By byStar = By.xpath("//*[@class=\"PrincipalCredits__PrincipalCreditsPanelWideScreen-hdn81t-0 iGxbgr\"] //*[contains(text(),\"Stars\")]/following-sibling::*");
		
		List <WebElement> listOfStars = driver.findElements(byStar);
		
		int size = listOfStars.size();
		
		boolean flagActorFound = false;
		for(int i=0;i<size;i++) {
			WebElement weOfList = listOfStars.get(i);
			String actualStar = weOfList.getText();
			System.out.println("Actor name found = " + actualStar );
			if (actualStar.contains(starName)) {
				flagActorFound = true;
				// as soon as actor is found break
				break;
			}

		}
		Assert.assertTrue(flagActorFound, "Actor name not found");
		
		System.out.println("Director Found is = " + flagDirector);
		System.out.println("Actor Found is = " + flagActorFound);
		
	}

	@DataProvider
	public Object[][] dp() throws IOException {

		String newArray[][]=null;
		
		String fileName = "src\\test\\resources\\data\\ImdbDay2.xlsx";
		String sheetName = "Sheet1";
		
		newArray = utils.DataReaders.getExcelDataUsingPoi(fileName, sheetName);
		
		return newArray;

	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}

}
