package day2;

import org.testng.annotations.Test;

import utils.HelperFunctions;

import org.testng.annotations.BeforeTest;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;

public class SampleDropDownTNG {
	
 WebDriver driver;	
 @BeforeTest
 public void beforeTest() {
	  
		driver = HelperFunctions.createAppropriateDriver("chrome");
		// Change the file name to your own location
		driver.get("file:///dropdown.html"); // open the file in chrome and copy the url here if there is an exception
 }
	
  @Test
  public void f() {
	  
	// /html/body/form/select
	//  /html/body/form/select
	  
	  By bySelect = By.xpath("/html/body/form/select");
	  WebElement weSelect = driver.findElement(bySelect);
	  
	  Select selectObject = new Select(weSelect);
	  
	  List<WebElement> listWe = selectObject.getOptions();
	  
	  for (int i=0;i<listWe.size();i++) {
		   System.out.println(listWe.get(i).getText());
	  }
	  
	  selectObject.selectByIndex(1);
	  selectObject.selectByValue("4");
	  selectObject.selectByVisibleText("CP-SAT");
		
  }

  @AfterTest
  public void afterTest() {
	  
	  driver.quit();
  }

}
