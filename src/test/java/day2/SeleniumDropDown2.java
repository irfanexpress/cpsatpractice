package day2;

import org.testng.annotations.Test;

import utils.HelperFunctions;
import utils.MoreHelperFunctions;

import org.testng.annotations.BeforeTest;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;

/*
 * Exercise
 * Go to https://www.vodafone.in/
 * click on About us
 * Print which is the default selected Value in the dropdown
 * Select options in the dropDown
 * Assam
 * Haryana
 * WestBengal
 */

public class SeleniumDropDown2 {

	WebDriver driver;

	@BeforeTest
	public void beforeTest() {

		driver = HelperFunctions.createAppropriateDriver("chrome");
		driver.get("https://www.seleniumeasy.com/test");
	}
	
	
	@Test
	public void f() throws Exception {

		//Click Input Forms
		By menu1 = By.xpath("//a[normalize-space()='Input Forms']");
		WebElement weMenu1 = driver.findElement(menu1);

		weMenu1.click();
		
		//Click Dropdown List
				By subMenu = By.xpath("//a[normalize-space()='Select Dropdown List']");
				WebElement weSubMenu = driver.findElement(subMenu);

		// scroll to the element
		MoreHelperFunctions.scrollUp(driver);
		
		try {
			
			weSubMenu.click();
			//org.openqa.selenium.ElementClickInterceptedException
		}
		catch (ElementClickInterceptedException exp) {
			
			System.out.println(exp.getMessage());
			
			 //if there is an exception due to overlay issues
			// directly use JavaScript click
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", weSubMenu);
		}

		// Select dropdown
		// //*[@id="ctl00_CU_ddlCircle"]
		//select[@id='select-demo']

		By byDropdown = By.xpath("//*[@id=\"select-demo\"]");
	
		WebElement weDropDown = driver.findElement(byDropdown);

		Select selectDD = new Select(weDropDown);

		System.out.println("Default Selection is" + selectDD.getFirstSelectedOption().getText());

		// find out which one is selected
		List<WebElement> optionlist =  selectDD.getOptions();

		int size = optionlist.size();

		for (int i = 0 ; i < size ; i++)
		{
			WebElement we = optionlist.get(i);
			System.out.println(we.getText());
		}

		//selectDD.

		selectDD.selectByIndex(4); 

		// below wait is meant to show the class
		// not needed otherwise
		Thread.sleep(5000);
		
		//Reintialize the webelement
		weDropDown = driver.findElement(byDropdown);

		selectDD = new Select(weDropDown);		
		
		selectDD.selectByValue("Wednesday");

		//Reintialize the webelement
		weDropDown = driver.findElement(byDropdown);

		selectDD = new Select(weDropDown);		
		
		selectDD.selectByVisibleText("Sunday");

		
	}



	@AfterTest
	public void afterTest() {

		driver.quit();
	}

}
